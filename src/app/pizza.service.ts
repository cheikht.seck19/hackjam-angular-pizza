import {Injectable, Output, EventEmitter} from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PizzaService {
  constructor() {}

  get() {}

  update() {}

  reset() {}

  getMaxItems(): Number {
    return environment.maxItems;
  }

}
