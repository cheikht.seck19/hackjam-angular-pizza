import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostListener
} from "@angular/core";
import { PIZZAS } from "../pizzasList";
import { BasketService } from "../basket.service";
import { Pizza } from "../pizza";
import { PizzaService } from '../pizza.service';

@Component({
  selector: "app-pizzalist",
  templateUrl: "./pizzalist.component.html",
  styleUrls: ["./pizzalist.component.css"]
})
export class PizzalistComponent implements OnInit {
  @Input() pizzas: Pizza[];
  @Output() isAdded = new EventEmitter<boolean>();
  maxItems: Number;
  constructor(private basketService: BasketService, private pizzaService: PizzaService) {}

  ngOnInit() {
    this.maxItems = this.pizzaService.getMaxItems();
  }
  // pizzas = PIZZAS;
  pizzaList: Pizza[] = [];

  updateList(pizza: Pizza, addedToTotal: boolean) {
    this.basketService.addToTotalAmount(pizza.price, addedToTotal);
    this.isAdded.emit(addedToTotal);
  }

  decrementNumber(pizza: Pizza) {
    // Decrement the number of the ordered pizza
    // the total amount of the selected pizza should be reduced as well
    if (pizza.numberOrdered > 0) {
      pizza.numberOrdered -= 1;
      pizza.totalAmountProduct -= pizza.price;
      if (pizza.numberOrdered === 0) {
        this.pizzaList.splice(this.pizzaList.indexOf(pizza));
      }
    }
    // call the update list
    this.updateList(pizza, false);
  }

  incrementNumber(pizza: Pizza) {
    // Increment the number of the ordered pizza
    // the total amount of the selected pizza should be augmented as well
    pizza.numberOrdered += 1;
    pizza.totalAmountProduct += pizza.price;
    if (pizza.numberOrdered === 1) {
      this.pizzaList.push(pizza);
    }
    // call the update list
    this.updateList(pizza, true);
  }
}

